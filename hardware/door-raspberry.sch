EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D1
U 1 1 5E2E9FD3
P 8150 3150
F 0 "D1" H 8143 3367 50  0000 C CNN
F 1 "LED" H 8143 3276 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 8150 3150 50  0001 C CNN
F 3 "~" H 8150 3150 50  0001 C CNN
	1    8150 3150
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5E2EB70F
P 1000 2850
F 0 "SW1" H 1000 3135 50  0000 C CNN
F 1 "SW_Push" H 1000 3044 50  0000 C CNN
F 2 "Button_Switch_THT:Nidec_Copal_SH-7010C" H 1000 3050 50  0001 C CNN
F 3 "~" H 1000 3050 50  0001 C CNN
	1    1000 2850
	0    -1   -1   0   
$EndComp
$Comp
L MCU_Microchip_PIC12:74AHCV17A U1
U 1 1 5E329DD9
P 5050 2300
F 0 "U1" H 5050 2867 50  0000 C CNN
F 1 "74AHCV17A" H 5050 2776 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5150 2950 50  0001 C CNN
F 3 "https://www.mouser.de/datasheet/2/916/74AHCV17A-1597624.pdf" H 5150 2200 50  0001 C CNN
	1    5050 2300
	1    0    0    -1  
$EndComp
$Comp
L Timer:LM555xN U2
U 1 1 5E3A8C23
P 7750 2400
F 0 "U2" H 7750 2981 50  0000 C CNN
F 1 "LM555xN" H 7750 2890 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 8400 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm555.pdf" H 8600 2000 50  0001 C CNN
	1    7750 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C3
U 1 1 5E3AA0AA
P 1400 2900
F 0 "C3" H 1518 2946 50  0000 L CNN
F 1 "CP" H 1518 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 1438 2750 50  0001 C CNN
F 3 "~" H 1400 2900 50  0001 C CNN
	1    1400 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5E3AA868
P 6500 2500
F 0 "C2" H 6618 2546 50  0000 L CNN
F 1 "CP" H 6618 2455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 6538 2350 50  0001 C CNN
F 3 "~" H 6500 2500 50  0001 C CNN
	1    6500 2500
	1    0    0    -1  
$EndComp
NoConn ~ 1350 800 
Text Label 1550 650  3    50   ~ 0
GND
Text Label 1450 650  3    50   ~ 0
5V
$Comp
L Device:R R5
U 1 1 5E3DE2F4
P 1250 2550
F 0 "R5" V 1043 2550 50  0000 C CNN
F 1 "R" V 1134 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1180 2550 50  0001 C CNN
F 3 "~" H 1250 2550 50  0001 C CNN
	1    1250 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5E3DE8E1
P 1000 2300
F 0 "R2" H 1070 2346 50  0000 L CNN
F 1 "R" H 1070 2255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 930 2300 50  0001 C CNN
F 3 "~" H 1000 2300 50  0001 C CNN
	1    1000 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 1900 1000 2150
Wire Wire Line
	1000 2450 1000 2550
Wire Wire Line
	1000 2550 1100 2550
Connection ~ 1000 2550
Wire Wire Line
	1000 2550 1000 2650
Wire Wire Line
	1400 2550 1400 2750
Text Label 1500 2550 0    50   ~ 0
MOR
Wire Wire Line
	1400 2550 1500 2550
Connection ~ 1400 2550
Wire Wire Line
	1550 800  1550 650 
Wire Wire Line
	1450 800  1450 650 
Wire Wire Line
	1000 3050 1000 3150
Wire Wire Line
	1000 3150 1400 3150
Wire Wire Line
	1400 3150 1400 3050
Wire Wire Line
	1000 3150 850  3150
Wire Wire Line
	850  3150 850  2000
Connection ~ 1000 3150
Text Notes 1200 2250 0    50   ~ 0
MOR\nManual Override\n
$Comp
L Switch:SW_Push SW2
U 1 1 5E441CBF
P 2050 2850
F 0 "SW2" H 2050 3135 50  0000 C CNN
F 1 "SW_Push" H 2050 3044 50  0000 C CNN
F 2 "Button_Switch_THT:Nidec_Copal_SH-7010C" H 2050 3050 50  0001 C CNN
F 3 "~" H 2050 3050 50  0001 C CNN
	1    2050 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C4
U 1 1 5E441CC5
P 2450 2900
F 0 "C4" H 2568 2946 50  0000 L CNN
F 1 "CP" H 2568 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 2488 2750 50  0001 C CNN
F 3 "~" H 2450 2900 50  0001 C CNN
	1    2450 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5E441CCB
P 2300 2550
F 0 "R6" V 2093 2550 50  0000 C CNN
F 1 "R" V 2184 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2230 2550 50  0001 C CNN
F 3 "~" H 2300 2550 50  0001 C CNN
	1    2300 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5E441CD1
P 2050 2300
F 0 "R3" H 2120 2346 50  0000 L CNN
F 1 "R" H 2120 2255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1980 2300 50  0001 C CNN
F 3 "~" H 2050 2300 50  0001 C CNN
	1    2050 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1900 2050 2150
Wire Wire Line
	2050 2450 2050 2550
Wire Wire Line
	2050 2550 2150 2550
Connection ~ 2050 2550
Wire Wire Line
	2050 2550 2050 2650
Wire Wire Line
	2450 2550 2450 2750
Text Label 2550 2550 0    50   ~ 0
DOOR
Wire Wire Line
	2450 2550 2550 2550
Connection ~ 2450 2550
Wire Wire Line
	2050 3050 2050 3150
Wire Wire Line
	2050 3150 2450 3150
Wire Wire Line
	2450 3150 2450 3050
Wire Wire Line
	2050 3150 1900 3150
Wire Wire Line
	1900 3150 1900 2000
Connection ~ 2050 3150
Text Notes 2250 2250 0    50   ~ 0
DOOR\nTür Knopf
$Comp
L Switch:SW_Push SW3
U 1 1 5E445454
P 3100 2850
F 0 "SW3" H 3100 3135 50  0000 C CNN
F 1 "SW_Push" H 3100 3044 50  0000 C CNN
F 2 "TerminalBlock_MetzConnect:TerminalBlock_MetzConnect_Type011_RT05502HBWC_1x02_P5.00mm_Horizontal" H 3100 3050 50  0001 C CNN
F 3 "~" H 3100 3050 50  0001 C CNN
	1    3100 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C5
U 1 1 5E44545A
P 3500 2900
F 0 "C5" H 3618 2946 50  0000 L CNN
F 1 "CP" H 3618 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 3538 2750 50  0001 C CNN
F 3 "~" H 3500 2900 50  0001 C CNN
	1    3500 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5E445460
P 3350 2550
F 0 "R7" V 3143 2550 50  0000 C CNN
F 1 "R" V 3234 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3280 2550 50  0001 C CNN
F 3 "~" H 3350 2550 50  0001 C CNN
	1    3350 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5E445466
P 3100 2300
F 0 "R4" H 3170 2346 50  0000 L CNN
F 1 "R" H 3170 2255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3030 2300 50  0001 C CNN
F 3 "~" H 3100 2300 50  0001 C CNN
	1    3100 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1900 3100 2150
Wire Wire Line
	3100 2450 3100 2550
Wire Wire Line
	3100 2550 3200 2550
Connection ~ 3100 2550
Wire Wire Line
	3100 2550 3100 2650
Wire Wire Line
	3500 2550 3500 2750
Text Label 3600 2550 0    50   ~ 0
SMO
Wire Wire Line
	3500 2550 3600 2550
Connection ~ 3500 2550
Wire Wire Line
	3100 3050 3100 3150
Wire Wire Line
	3100 3150 3500 3150
Wire Wire Line
	3500 3150 3500 3050
Wire Wire Line
	3100 3150 2950 3150
Wire Wire Line
	2950 3150 2950 2000
Connection ~ 3100 3150
Text Notes 3300 2250 0    50   ~ 0
SMO\nSet MOR
Wire Wire Line
	1000 1900 2050 1900
Wire Wire Line
	850  2000 1900 2000
Connection ~ 2050 1900
Wire Wire Line
	2050 1900 3100 1900
Connection ~ 1900 2000
Wire Wire Line
	1900 2000 2950 2000
Wire Notes Line
	3800 3350 3800 1650
Wire Notes Line
	3800 1650 650  1650
Wire Notes Line
	650  1650 650  3350
Wire Notes Line
	650  3350 3800 3350
Text Label 1050 2000 0    50   ~ 0
GND
Text Label 1050 1900 0    50   ~ 0
5V
Wire Wire Line
	7750 2800 7750 3150
Wire Wire Line
	6500 2650 6500 3150
Wire Wire Line
	7250 2600 6850 2600
Text Label 6850 2600 0    50   ~ 0
FL_MOR
Wire Wire Line
	6500 3150 6750 3150
NoConn ~ 7250 2400
Wire Wire Line
	8250 2600 8300 2600
Wire Wire Line
	7150 2850 7150 2200
Wire Wire Line
	7150 2200 7250 2200
Wire Wire Line
	8300 2600 8300 2850
Wire Wire Line
	8300 2850 7150 2850
$Comp
L Device:CP C1
U 1 1 5E5009D8
P 7000 2200
F 0 "C1" V 6745 2200 50  0000 C CNN
F 1 "CP" V 6836 2200 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 7038 2050 50  0001 C CNN
F 3 "~" H 7000 2200 50  0001 C CNN
	1    7000 2200
	0    1    1    0   
$EndComp
Connection ~ 7150 2200
Wire Wire Line
	6850 2200 6750 2200
Wire Wire Line
	6750 3150 6750 2200
Connection ~ 6750 3150
Wire Wire Line
	6750 3150 7750 3150
Text Label 6550 3150 0    50   ~ 0
GND
$Comp
L Device:R R1
U 1 1 5E50D237
P 8150 2000
F 0 "R1" V 7943 2000 50  0000 C CNN
F 1 "R" V 8034 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8080 2000 50  0001 C CNN
F 3 "~" H 8150 2000 50  0001 C CNN
	1    8150 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5E50EC1D
P 8500 2600
F 0 "RV1" H 8430 2554 50  0000 R CNN
F 1 "R_POT" H 8430 2645 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Omeg_PC16BU_Horizontal" H 8500 2600 50  0001 C CNN
F 3 "~" H 8500 2600 50  0001 C CNN
	1    8500 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8250 2400 8400 2400
NoConn ~ 8650 2600
Wire Notes Line
	9000 3350 6250 3350
Wire Wire Line
	6500 2000 6500 2350
Wire Notes Line
	9000 3350 9000 1700
Wire Notes Line
	9000 1700 6250 1700
Wire Notes Line
	6250 1700 6250 3350
Wire Wire Line
	8400 2000 8400 2400
Wire Wire Line
	8400 2000 8300 2000
Wire Wire Line
	8300 2600 8350 2600
Connection ~ 8300 2600
Wire Wire Line
	8400 2400 8500 2400
Wire Wire Line
	8500 2400 8500 2450
Connection ~ 8400 2400
Wire Wire Line
	8250 2200 8850 2200
Wire Wire Line
	8850 2200 8850 3150
Wire Wire Line
	8850 3150 8600 3150
$Comp
L Device:R R8
U 1 1 5E5AD4B8
P 8450 3150
F 0 "R8" V 8243 3150 50  0000 C CNN
F 1 "R" V 8334 3150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8380 3150 50  0001 C CNN
F 3 "~" H 8450 3150 50  0001 C CNN
	1    8450 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 3150 7750 3150
Connection ~ 7750 3150
Text Label 4300 2600 0    50   ~ 0
GND
Wire Wire Line
	4450 2600 4300 2600
NoConn ~ 5650 2600
NoConn ~ 5650 2500
NoConn ~ 4450 2500
NoConn ~ 4450 2400
Wire Wire Line
	5950 2100 5650 2100
Wire Wire Line
	5950 2200 5650 2200
NoConn ~ 5650 2400
NoConn ~ 5650 2300
Text Label 4100 2200 0    50   ~ 0
DOOR
Text Label 4100 2300 0    50   ~ 0
FL_DOOR
Text Label 5950 2200 2    50   ~ 0
FL_MOR
Text Label 5950 2100 2    50   ~ 0
MOR
Text Label 4100 2100 0    50   ~ 0
FL_SMO
Text Label 4100 2000 0    50   ~ 0
SMO
Wire Wire Line
	4100 2300 4450 2300
Wire Wire Line
	4100 2200 4450 2200
Wire Wire Line
	4450 2100 4100 2100
Wire Wire Line
	4100 2000 4450 2000
Wire Notes Line
	6050 2850 4000 2850
Wire Notes Line
	6050 1650 4000 1650
$Comp
L Device:R R9
U 1 1 5E7B3919
P 4900 3200
F 0 "R9" V 4693 3200 50  0000 C CNN
F 1 "R" V 4784 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4830 3200 50  0001 C CNN
F 3 "~" H 4900 3200 50  0001 C CNN
	1    4900 3200
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5E7B41EC
P 5200 3200
F 0 "D2" H 5193 2945 50  0000 C CNN
F 1 "LED" H 5193 3036 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 5200 3200 50  0001 C CNN
F 3 "~" H 5200 3200 50  0001 C CNN
	1    5200 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	5350 3200 5750 3200
Wire Wire Line
	4350 3200 4750 3200
Text Label 5750 3200 2    50   ~ 0
GND
Text Label 4350 3200 0    50   ~ 0
FL_SMO
Wire Notes Line
	6050 3350 4000 3350
Wire Notes Line
	6050 1650 6050 3350
Wire Notes Line
	4000 1650 4000 3350
Text Label 1650 500  3    50   ~ 0
FL_DOOR
Text Label 1750 500  3    50   ~ 0
FL_MOR
Text Label 1850 500  3    50   ~ 0
FL_SMO
Wire Wire Line
	1850 500  1850 800 
Wire Wire Line
	1750 500  1750 800 
Wire Wire Line
	1650 500  1650 800 
Text Label 1450 1600 1    50   ~ 0
E
Text Label 1550 1600 1    50   ~ 0
RS
Wire Wire Line
	1450 1300 1450 1600
Wire Wire Line
	1550 1300 1550 1600
NoConn ~ 1950 800 
NoConn ~ 2250 800 
NoConn ~ 2050 800 
NoConn ~ 2150 800 
NoConn ~ 1350 1300
NoConn ~ 2150 1300
NoConn ~ 2250 1300
Wire Wire Line
	1950 1300 1950 1600
Wire Wire Line
	2050 1300 2050 1600
Wire Wire Line
	1650 1300 1650 1600
Wire Wire Line
	1850 1300 1850 1600
Text Label 2050 1600 1    50   ~ 0
DATA_3
Text Label 1950 1600 1    50   ~ 0
DATA_2
Text Label 1850 1600 1    50   ~ 0
DATA_1
Text Label 1650 1600 1    50   ~ 0
DATA_0
NoConn ~ 2350 5650
NoConn ~ 2250 5650
NoConn ~ 2150 5650
NoConn ~ 2050 5650
Wire Wire Line
	2850 6450 2850 6600
Wire Wire Line
	2850 6600 1350 6600
Wire Wire Line
	2750 5650 2750 5600
Wire Wire Line
	1550 5600 2750 5600
Wire Wire Line
	2650 5650 2650 5550
Wire Wire Line
	2650 5550 1450 5550
Wire Wire Line
	1650 5500 2850 5500
Wire Wire Line
	2850 5500 2850 5650
Wire Wire Line
	1950 5650 1950 5450
Wire Wire Line
	1950 5450 2150 5450
Wire Wire Line
	1850 5650 1850 5400
Wire Wire Line
	1850 5400 2250 5400
Wire Wire Line
	1650 5650 1700 5650
Wire Wire Line
	1700 5650 1700 5300
Wire Wire Line
	1700 5300 2450 5300
Wire Wire Line
	1750 5650 1750 5350
Wire Wire Line
	1750 5350 2350 5350
Wire Wire Line
	2550 6450 2550 6500
Wire Wire Line
	2550 6500 3100 6500
Wire Wire Line
	3100 6500 3100 5650
Wire Wire Line
	2450 6450 2450 6550
Wire Wire Line
	2450 6550 3050 6550
Wire Wire Line
	3150 6550 3150 5600
Text Notes 3100 5600 0    50   Italic 0
Pinout:\nGND: 1, 3, 5, 16\n5V:   2, 15\nGPIO:\n  4 ->04, 6 ->16,\n  11->18, 12->22,\n  13->23, 14->24\nNC: 7,8,9,10\n\n
Wire Wire Line
	2550 5450 2900 5450
Wire Wire Line
	2650 5400 2950 5400
Wire Wire Line
	2950 5400 2950 5600
Wire Wire Line
	2950 5600 3150 5600
Wire Wire Line
	2900 5450 2900 5650
Wire Wire Line
	2900 5650 3100 5650
Wire Notes Line
	4050 4450 4050 6800
Wire Notes Line
	900  4450 900  6800
Text Notes 950  4600 0    69   Italic 14
Expansion Modul LCD-Display (wahrscheinlich optional)
Wire Notes Line
	900  4450 4050 4450
Wire Notes Line
	900  6800 4050 6800
Wire Wire Line
	1150 3900 1350 3900
Wire Wire Line
	1350 3900 1550 3900
Connection ~ 1350 3900
Connection ~ 1550 3900
Wire Wire Line
	1550 3900 2650 3900
Text Label 1200 3900 2    50   ~ 0
GND
Text Label 1250 3800 2    50   ~ 0
5V
Text Label 1650 3750 3    50   ~ 0
E
Text Label 1450 3750 3    50   ~ 0
RS
Text Label 2150 3850 3    50   ~ 0
DATA_0
Text Label 2250 3850 3    50   ~ 0
DATA_1
Text Label 2350 3850 3    50   ~ 0
DATA_2
Text Label 2450 3850 3    50   ~ 0
DATA_3
Wire Wire Line
	3050 6050 3050 6550
Wire Wire Line
	3050 6650 1150 6650
$Comp
L power:GND #PWR01
U 1 1 5E4CA2FD
P 1000 6650
F 0 "#PWR01" H 1000 6400 50  0001 C CNN
F 1 "GND" H 1005 6477 50  0000 C CNN
F 2 "" H 1000 6650 50  0001 C CNN
F 3 "" H 1000 6650 50  0001 C CNN
	1    1000 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 6050 1250 6050
$Comp
L Display_Character:WC1602A DS?
U 1 1 5E466111
P 2250 6050
AR Path="/5E3D255C/5E466111" Ref="DS?"  Part="1" 
AR Path="/5E466111" Ref="DS1"  Part="1" 
F 0 "DS1" V 2204 6894 50  0000 L CNN
F 1 "WC1602A" V 2295 6894 50  0000 L CNN
F 2 "Display:WC1602A" H 2250 5150 50  0001 C CIN
F 3 "http://www.wincomlcd.com/pdf/WC1602A-SFYLYHTC06.pdf" H 2950 6050 50  0001 C CNN
	1    2250 6050
	0    1    1    0   
$EndComp
Connection ~ 3050 6550
Wire Wire Line
	3050 6550 3150 6550
Wire Wire Line
	3050 6550 3050 6650
Wire Wire Line
	1000 6650 1150 6650
Connection ~ 1150 6650
Wire Wire Line
	7500 2000 7500 1750
Wire Wire Line
	7500 1750 7750 1750
Wire Wire Line
	7950 1750 7950 2000
Wire Wire Line
	7950 2000 8000 2000
Wire Wire Line
	7750 2000 7750 1750
Connection ~ 7750 1750
Wire Wire Line
	7750 1750 7950 1750
Text Label 5950 2000 2    50   ~ 0
5V
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5E4D991B
P 6700 2000
F 0 "#FLG01" H 6700 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 6700 2173 50  0000 C CNN
F 2 "" H 6700 2000 50  0001 C CNN
F 3 "~" H 6700 2000 50  0001 C CNN
	1    6700 2000
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5E4DD685
P 1250 3650
F 0 "#FLG02" H 1250 3725 50  0001 C CNN
F 1 "PWR_FLAG" H 1250 3823 50  0000 C CNN
F 2 "" H 1250 3650 50  0001 C CNN
F 3 "~" H 1250 3650 50  0001 C CNN
	1    1250 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2000 5950 2000
$Comp
L Device:R_POT RV2
U 1 1 5E53CEE4
P 2550 3650
F 0 "RV2" H 2480 3604 50  0000 R CNN
F 1 "R_POT" H 2480 3695 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Omeg_PC16BU_Horizontal" H 2550 3650 50  0001 C CNN
F 3 "~" H 2550 3650 50  0001 C CNN
	1    2550 3650
	-1   0    0    1   
$EndComp
NoConn ~ 2550 3500
Wire Wire Line
	2400 3650 1250 3650
Connection ~ 1250 3650
NoConn ~ 1750 1300
Wire Wire Line
	2650 3900 2650 5400
Wire Wire Line
	2250 3850 2250 5400
Wire Wire Line
	2350 3850 2350 5350
Wire Wire Line
	2450 3850 2450 5300
Wire Wire Line
	2550 3800 2550 5450
Wire Wire Line
	1350 3900 1350 6600
Wire Wire Line
	1550 3900 1550 5600
Wire Wire Line
	1650 3750 1650 5500
Wire Wire Line
	1450 3750 1450 5550
Wire Wire Line
	1150 3900 1150 6650
Wire Wire Line
	2150 3850 2150 5450
Wire Wire Line
	1250 3650 1250 6050
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J1
U 1 1 5E4557F7
P 1750 1100
F 0 "J1" V 1846 512 50  0000 R CNN
F 1 "Conn_02x10_Odd_Even" V 1755 512 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 1750 1100 50  0001 C CNN
F 3 "~" H 1750 1100 50  0001 C CNN
	1    1750 1100
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5E4828DE
P 650 650
F 0 "H2" H 750 696 50  0000 L CNN
F 1 "MountingHole" H 750 605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 650 650 50  0001 C CNN
F 3 "~" H 650 650 50  0001 C CNN
	1    650  650 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5E4846E6
P 9300 600
F 0 "H1" H 9400 646 50  0000 L CNN
F 1 "MountingHole" H 9400 555 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 9300 600 50  0001 C CNN
F 3 "~" H 9300 600 50  0001 C CNN
	1    9300 600 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5E49066B
P 9100 6400
F 0 "H4" H 9200 6446 50  0000 L CNN
F 1 "MountingHole" H 9200 6355 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 9100 6400 50  0001 C CNN
F 3 "~" H 9100 6400 50  0001 C CNN
	1    9100 6400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5E490DC7
P 600 6400
F 0 "H3" H 700 6446 50  0000 L CNN
F 1 "MountingHole" H 700 6355 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 600 6400 50  0001 C CNN
F 3 "~" H 600 6400 50  0001 C CNN
	1    600  6400
	1    0    0    -1  
$EndComp
Text Label 6800 2000 0    50   ~ 0
5V
Connection ~ 6700 2000
Wire Wire Line
	6700 2000 7500 2000
Wire Wire Line
	6500 2000 6700 2000
$EndSCHEMATC
