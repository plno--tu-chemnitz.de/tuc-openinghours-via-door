# tuc openinghours via door

  
Automatic opening hours / status by unlocking the office with manual override option :D
includes hardware files (kicad) & test scripts


<img src="/pcb-with-parts.jpg" alt="pcb"
  title="3D-Visualisierung des Boards" width="640" height="315" />


#### Project Setup:
Used tool is kicad. The kicad-project will be under the folder `/hardware`
The test scripts are in `/test-scripts` divided into programming languages\
  (we might want to use elixir or elixir-nerves in a later stage\
  as the rasbpi could be useful for the hosting the [stim-project](https://gitlab.hrz.tu-chemnitz.de/plno--tu-chemnitz.de/stim) (fsrif-meeting-protocol-tool) )
  
  
  
#### TODO / Roadmap (might be concurrent):
- [x] Create symbols for used parts
  - [x] if needed create new footprints
- [x] Make a rough sketch how it should behave and which parts are needed
  - [x] Order sample parts for breadboard testing
    - [x] buy schmitt trigger maybe for nice signal flanks. Alternative would be to make it software base but eh 40ct-80ct more is quite a good deal for not being dependant on software 
    - [x] Test if everything works via breadboard
- [x] Design Schema (Layout)
  - [x] Connect pcb footprints ( traces )
  - [x] Order a pack (5-10) at jclpcb
  - [ ] Wait 1-2 weeks for production and shipping ... ~doing
  - [ ] Solder 
- [ ] Test soldered pcb
  - [ ] Present Achievements to other student councils :DD

#### Questions / How to contribute:

just make an issue [here](https://gitlab.hrz.tu-chemnitz.de/plno--tu-chemnitz.de/tuc-openinghours-via-door/issues/new) or send an email to [me](mailto:plno@hrz.tu-chemnitz.de).\
The prefered email subject would be `[Gitlab: openinghours via door]`
